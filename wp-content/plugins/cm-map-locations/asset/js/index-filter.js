jQuery(function($) {
	
	$('.cmloc-location-filter-category').change(function() {
		var obj = $(this);
		var filter = obj.parents('.cmloc-location-index-filter');
		var s = filter.find('.cmloc-input-search').val();
		location.href = obj.val() + (s ? '?s=' + encodeURIComponent(s) : '');
	});
	
});