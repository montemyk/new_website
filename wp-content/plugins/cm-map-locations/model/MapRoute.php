<?php

namespace com\cminds\maplocations\model;

use com\cminds\maplocations\controller\RouteController;

class MapRoute extends PostType {
	
	const POST_TYPE = 'cmmrm_route';
	const LOCATION_POST_TYPE = 'cmmrm_location';
	const LOCATION_META_LAT = '_cmmrm_latitude';
	const LOCATION_META_LONG = '_cmmrm_longitude';
	const META_PATH_COLOR = '_cmmrm_path_color';
	const META_OVERVIEW_PATH = '_cmmrm_overview_path';
	
	
	static function registerPostType() {
		// Don't
	}
	
	
	function getCategories($fields = TaxonomyTerm::FIELDS_MODEL, $params = array()) {
		return MapRoutesCategory::getPostTerms($this->getId(), $fields, $params);
	}
	
	static function getIndexMapJSLocations() {
		global $wpdb;
		
		$joinString = '';
		$whereString = '';
	
		$sql = $wpdb->prepare("SELECT r.ID AS id, r.post_title AS name, r.post_type, lm_lat.meta_value AS lat, lm_lon.meta_value AS `long`,
			rm_pc.meta_value AS `pathColor`, rm_op.meta_value AS `path`
			FROM $wpdb->posts r
			JOIN $wpdb->posts l ON l.post_parent = r.ID AND l.post_type IN (%s, %s) AND l.menu_order = 1
			JOIN $wpdb->postmeta lm_lat ON lm_lat.post_id = l.ID AND lm_lat.meta_key IN (%s, %s)
			JOIN $wpdb->postmeta lm_lon ON lm_lon.post_id = l.ID AND lm_lon.meta_key IN (%s, %s)
			LEFT JOIN $wpdb->postmeta rm_pc ON rm_pc.post_id = r.ID AND rm_pc.meta_key IN (%s, %s)
			LEFT JOIN $wpdb->postmeta rm_op ON rm_op.post_id = r.ID AND rm_op.meta_key IN (%s, %s)
			$joinString
			WHERE r.post_type IN (%s, %s) AND r.post_status = 'publish' $whereString
			",
			Location::POST_TYPE,
			self::LOCATION_POST_TYPE,
			Location::META_LAT,
			self::LOCATION_META_LAT,
			Location::META_LONG,
			self::LOCATION_META_LONG,
			Route::META_PATH_COLOR,
			self::META_PATH_COLOR,
			Route::META_OVERVIEW_PATH,
			self::META_OVERVIEW_PATH,
			Route::POST_TYPE,
			self::POST_TYPE
		);
	
		$routes = $wpdb->get_results($sql, ARRAY_A);
	
		foreach ($routes as $i => $row) {
			$routes[$i]['permalink'] = get_permalink($row['id']);
			$routes[$i]['type'] = Location::TYPE_LOCATION;
			if ($row['post_type'] == Route::POST_TYPE) {
				/* @var $route Route */
				$route = Route::getInstance($row['id']);
				$routes[$i]['icon'] = $route->getIconUrl();
				$routes[$i]['infowindow'] = RouteController::getInfoWindowView($route);
				$routes[$i]['categories'] = $route->getCategories(TaxonomyTerm::FIELDS_ID_NAME);
// 				var_dump($row['id']);
// 				var_dump($routes[$i]['categories']);
				Route::clearInstances();
			} else {
				/* @var $route MapRoute */
				$route = MapRoute::getInstance($row['id']);
				$routes[$i]['infowindow'] = self::getInfoWindow($row['id']);
// 				var_dump($row['id']);
// 				var_dump($route->getId());
				$routes[$i]['categories'] = $route->getCategories(TaxonomyTerm::FIELDS_ID_NAME);
			}
			unset($row['post_type']);
		}
	
		return $routes;
	
	}
	
	
	static function getInfoWindow($id) {
		$route = self::getInstance($id);
		return sprintf('<div class="cmloc-infowindow"><h2><a href="%s">%s</a></h2>%s</div>',
			esc_attr($route->getPermalink()),
			esc_html($route->getTitle()),
			$route->getContent()
		);
	}
	
	
}
