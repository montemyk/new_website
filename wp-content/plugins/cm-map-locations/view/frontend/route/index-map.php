<?php


use com\cminds\maplocations\model\Labels;

?><div class="cmloc-location-index-map">

	<div id="cmloc-location-index-map-canvas"></div>
	<script type="text/javascript">
	jQuery(function($) {
		new CMLOC_Index_Map('cmloc-location-index-map-canvas', <?php echo json_encode($routes); ?>);
	});
	</script>
	
</div>