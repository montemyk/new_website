<?php

use com\cminds\maplocations\controller\DashboardController;

use com\cminds\maplocations\controller\FrontendController;

use com\cminds\maplocations\controller\RouteController;

use com\cminds\maplocations\controller\SettingsController;
use com\cminds\maplocations\view\SettingsView;
use com\cminds\maplocations\App;
use com\cminds\maplocations\model\Settings;


if (!empty($_GET['status']) AND !empty($_GET['msg'])) {
	printf('<div id="message" class="%s"><p>%s</p></div>', ($_GET['status'] == 'ok' ? 'updated' : 'error'), esc_html($_GET['msg']));
}

$settingsView = new SettingsView();

?>

<div class="cmloc-help-shortcodes">
	<p><strong>Index page:</strong> <a href="<?php echo esc_attr(FrontendController::getUrl()); ?>" target="_blank"><?php echo FrontendController::getUrl(); ?></a></p>
	<p><strong>User dashboard:</strong> <a href="<?php echo esc_attr(RouteController::getDashboardUrl()); ?>" target="_blank"><?php echo RouteController::getDashboardUrl(); ?></a></p>
	<?php if (App::isPro()): ?>
		<h4>Shortcodes</h4>
		<ul>
			<li><strong>[cmloc-snippet id=route_id featured=image|map]</strong> - displays location's snippet.</li>
			<li><strong>[cmloc-location-map id=route_id graph=1 params=1]</strong> - displays location's map.</li>
			<li><strong>[cmloc-locations-map list=none|left|right|bottom|compact limit=5 page=1 category=id|slug]</strong>
				- displays the locations map, optionally from chosen category and with the pagination.</li>
			<li><strong>[cmmrm-cmloc-common-map path=0 categoryfilter=0]</strong> - displays common map with locations and routes from the CM Map Routes Manager plugin.</li>
			<li><strong>[cmloc-business category=id|slug categoryfilter=0]</strong> - displays map with the CM Business Directory records.</li>
		</ul>
	<?php endif; ?>
</div>

<form method="post" id="settings">

<ul class="cmloc-settings-tabs"><?php

$tabs = apply_filters('cmloc_settings_pages', Settings::$categories);
foreach ($tabs as $tabId => $tabLabel) {
	printf('<li><a href="#tab-%s">%s</a></li>', $tabId, $tabLabel);
}

?></ul>

<div class="inner"><?php

echo $settingsView->render();

?></div>

<p class="form-finalize">
	<a href="<?php echo esc_attr($clearCacheUrl); ?>" class="right button">Clear cache</a>
	<input type="hidden" name="nonce" value="<?php echo wp_create_nonce(SettingsController::getMenuSlug()); ?>" />
	<input type="submit" value="Save" class="button button-primary" />
</p>

</form>
