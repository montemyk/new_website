<?php get_header(); ?>
 

<div class="container">
<div>
<?php the_post_thumbnail('full', array('class' => 'img-responsive')); ?>
</div>
    <?php the_post(); ?>
    <div class="entry-content">
        <?php the_content(); ?>
        <?php wp_link_pages('before=<div class="page-link">' . __( 'Pages:', 'your-theme' ) . '&after=</div>') ?>
    </div><!-- .entry-content -->          
</div><!-- #container -->
 
<?php get_footer(); ?>

