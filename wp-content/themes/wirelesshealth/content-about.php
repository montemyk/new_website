
<?php
$args = array(
    'post_type' => 'home-widget',
    'order' => 'ASC'
);

$widgets = new WP_Query($args);
if ($widgets->have_posts()):
while ($widgets->have_posts()): $widgets->the_post();
    if(get_the_title() == 'WHY WAH?'){
?>


<div class="container">
    <div class="col-md-5 about-description"> 
        <?php the_post_thumbnail('medium', array('class' => 'img-responsive about-widget-img hvr-bob ')); ?>
    </div>
    <div class="col-md-7 about-widget-content">
        <div class="page-title text-left"><?php the_title(); ?></div>
        <div class="sub-content"><?php echo get_post_meta(get_the_ID(), 'wpcf-subtitle', true) ?></div>
        <div class="wow fadeInRight" data-wow-duration="2s">
            <?php the_content(); ?>
        </div>
        <a href="<?php echo site_url() . '/about-us'?>"><div class="learn-more">Learn More</div></a>
    </div>
</div>
<?php
    }
endwhile;
endif;
?>