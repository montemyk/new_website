<?php
/**
 *
 * Template Name: services
 * @package WordPress
 * @subpackage WAH WEBSITE
 * @since WAHSITE 1.0
 */
?>


<?php get_header(); ?>
 


<div class="container-fluid no-padding">

<ul class="bxslider bannerstory servicesbg">
	<?php 
		$args = array('post_type' => 'service', 'orderby' => 'date','order' => 'DESC');
        $slides = new WP_Query($args);
        if ($slides->have_posts()):
                while ($slides->have_posts()): $slides->the_post();
	?>
	  <li>
	  	<?php 
			$feat_image = wp_get_attachment_url(get_post_thumbnail_id());
		?>
		<div class="row">
		<div class="header-services col-md-6">
		        <div class="hvr-underline-from-left"><?php the_title(); ?></div>
		        </div>
		<div class="carousel-overlay col-md-7 services-space">
	  	<img src="<?php echo $feat_image; ?>" /></div>
	  	  <div class="services-content col-md-4" >
		        	<?php 
		        		the_content();
		        	?>
		        	</div>
		    <div class="header-services col-md-6">
		        <div class="hvr-underline-from-left"><?php the_title(); ?></div>
		        </div>
		      
		
		</div>
	  </li>
  <?php 
  endwhile;
  endif;
  ?>
</ul>
</div>
</div>
</div>


 
<?php get_footer(); ?>