<?php

add_theme_support( 'menus' );

function wah_scripts() {
    $datenow = date("YmdHis");
    wp_enqueue_style( 'wahstyle', get_template_directory_uri() . '/css/style.css', array(), $datenow );
    wp_enqueue_style( 'bootstrap', get_template_directory_uri() . '/css/bootstrap.css', array(), '1.0' );
    wp_enqueue_style( 'bootstrap-theme', get_template_directory_uri() . '/css/bootstrap-theme.css', array(), '1.0' );
    wp_enqueue_style( 'bxslider', get_template_directory_uri() . '/css/jquery.bxslider.css', array(), '1.0' );
    wp_enqueue_style( 'slick', get_template_directory_uri() . '/css/slick.css', array(), '1.0' );
    wp_enqueue_style( 'slick-theme', get_template_directory_uri() . '/css/slick-theme.css', array(), '1.0' );
    wp_enqueue_style( 'wow-css', get_template_directory_uri() . '/css/animate.css', array(), '1.0' );
    wp_enqueue_style( 'milestone-css', get_template_directory_uri() . '/css/milestone.css', array(), '1.0' );
    wp_enqueue_style( 'ihover-css', get_template_directory_uri() . '/css/ihover.css', array(), '1.0' );
       wp_enqueue_style( 'hover-css', get_template_directory_uri() . '/css/hover.css', array(), '1.0' );
     wp_enqueue_style( 'owl', get_template_directory_uri() . '/css/owl.carousel.css', array(), '1.0' );
         wp_enqueue_style( 'mosaic', get_template_directory_uri() . '/css/mosaic.css', array(), '1.0' );
  
    
    // Theme stylesheet.
    wp_enqueue_style( 'wah-style', get_stylesheet_uri() );

    // Load the html5 shiv.
    wp_enqueue_script( 'wah-script', get_template_directory_uri() . '/js/script.js', array( 'jquery' ), '2', true);
    wp_enqueue_script( 'wah-bootstrap', get_template_directory_uri() . '/js/bootstrap.js', array(), '1', true );
    wp_enqueue_script( 'wah-bxslider', get_template_directory_uri() . '/js/jquery.bxslider.js', array(), '1', true );   
    wp_enqueue_script( 'wah-slick', get_template_directory_uri() . '/js/slick.js', array(), '1', true );        
    wp_enqueue_script( 'wah-wow', get_template_directory_uri() . '/js/wow.js', array(), '1', true ); 
    wp_enqueue_script( 'light-wow', get_template_directory_uri() . '/js/lightslider.js', array(), '1', true );
    wp_enqueue_script( 'odometer', get_template_directory_uri() . '/js/odometer.js', array(), '1', true );
    wp_enqueue_script( 'jquery.counterup', get_template_directory_uri() . '/js/jquery.counterup.js', array(), '1', true );
    wp_enqueue_script( 'owl', get_template_directory_uri() . '/js/owl.carousel.js', array(), '1', true );
    wp_enqueue_script( 'mosaic', get_template_directory_uri() . '/js/mosaic.js', array(), '1', true );
    wp_enqueue_script( 'jquery-cycle2', get_template_directory_uri() . '/js/jquery.cycle2.js', array(), '1', true );
    wp_enqueue_script( 'carouselcycle', get_template_directory_uri() . '/js/jquery.cycle2.carousel.js', array(), '1', true );
  

        
}
add_action( 'wp_enqueue_scripts', 'wah_scripts' );

add_filter('show_admin_bar', '__return_false');

remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
remove_action( 'wp_print_styles', 'print_emoji_styles' );
remove_action( 'admin_print_styles', 'print_emoji_styles' );    
remove_action( 'wp_head', 'feed_links_extra'); // Display the links to the extra feeds such as category feeds
remove_action( 'wp_head', 'feed_links'); // Display the links to the general feeds: Post and Comment Feed
remove_action( 'wp_head', 'rsd_link'); // Display the link to the Really Simple Discovery service endpoint, EditURI link
remove_action( 'wp_head', 'wlwmanifest_link'); // Display the link to the Windows Live Writer manifest file.
remove_action( 'wp_head', 'index_rel_link'); // index link
remove_action( 'wp_head', 'parent_post_rel_link'); // prev link
remove_action( 'wp_head', 'start_post_rel_link'); // start link
remove_action( 'wp_head', 'adjacent_posts_rel_link'); // Display relational links for the posts adjacent to the current post.
remove_action( 'wp_head', 'wp_generator'); // Display the XHTML generator that is generated on the wp_head hook, WP version
remove_action( 'wp_head',      'wp_oembed_add_discovery_links'         );
remove_action( 'wp_head',      'rest_output_link_wp_head'              );
remove_action( 'wp_head', 'index_rel_link' ); // index link
remove_action( 'wp_head', 'parent_post_rel_link' ); // prev link
remove_action( 'wp_head', 'start_post_rel_link'); // start link
remove_action( 'wp_head', 'adjacent_posts_rel_link'); // Display relational links for the posts adjacent to the current post.
add_filter( 'index_rel_link', 'disable_stuff' );
add_filter( 'parent_post_rel_link', 'disable_stuff' );
add_filter( 'start_post_rel_link', 'disable_stuff' );
add_filter( 'previous_post_rel_link', 'disable_stuff' );
add_filter( 'next_post_rel_link', 'disable_stuff' );


function disable_stuff( $data ) {
    return false;
}

if ( function_exists( 'add_theme_support' ) ) {
add_theme_support( 'post-thumbnails' );
}

// Custom callback to list comments in the hbd-theme style
function custom_comments($comment, $args, $depth) {
  $GLOBALS['comment'] = $comment;
    $GLOBALS['comment_depth'] = $depth;
  ?>
    <li id="comment-<?php comment_ID() ?>">
        <div>
            <?php comment_author(); ?>
        </div>
        <div class="comment-meta">
            <?php printf(__('%1$s at %2$s', 'hbd-theme'),
                    get_comment_date(),
                    get_comment_time(), '</span>'); ?>
        </div>
        <div class="comment-content">
            <?php comment_text() ?>
        </div>
        
<?php } // end custom_comments



function reg_widgets_init(){

    register_sidebar(array(
        'name' => 'Sidebar',
        'id'   => 'sidebar',
        'description'   => 'This is the widgetized sidebar.',
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
        'after_widget'  => '</div>',
        'before_title'  => '<h4>',
        'after_title'   => '</h4>'
    ));
}
add_action( 'widgets_init', 'reg_widgets_init' );




?>