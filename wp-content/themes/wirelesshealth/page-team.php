
<?php
/**
 *
 * Template Name: MEET THE TEAM
 * @package WordPress
 * @subpackage WAH WEBSITE
 * @since WAHSITE 1.0
 */
?>


<?php get_header(); ?>
<div class="container">
<?php the_post_thumbnail('full', array('class' => 'img-responsive')); ?>
</div>

          <div class="container">
                  <div class="container entry-content">
                         <div class=" text-right  col-md-4">
                          <div class="widget-title">WHO WE ARE?</div>
                          <div>WAH collaborates with partners across the country to lead change in health care.
                               Together, WAH is transforming health care for a better future for everyone they serve. Their partners choose WAH for the combination their unique talents and capabilities.
          </div>
                <div class="position-container">
                  <a class="development position-type">Developer</a>
                  <a class="specialist position-type">Specialist</a>
                  <a class="supervisor position-type">Supervisor</a>
                  <a class="management position-type">Director</a>
                  <a class="associate position-type">Associate</a>
                </div>
        </div>

<div class="container">
          <div class="col-md-8">
              <?php 
                $args = array('post_type' => 'executive-director');
                $performer = new WP_Query($args);
                if ($performer->have_posts()):
                  while ($performer->have_posts()): $performer->the_post(); 
                  $profile = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID(), 'full'));
              ?>
                <div class="tp-container col-md-5 col-sm-3 col-xs-6">
                  <img class="team-img-all img-responsive  hvr-glow" src="<?php echo $profile[0]; ?>"/>  
                  <div class="t-name wow " data-wow-duration="2s"><?php the_title(); ?></div>
                  <div class="designation wow management teamfont" data-wow-duration="4s"><?php echo get_post_meta(get_the_ID(), 'wpcf-designation', true) ?></div>
                   <div class="fafa-designation"> <a target="_blank" href="http://localhost/wahsite/contact-us/"><i class="fa fa-mobile fa-top-header fa-lg" aria-hidden="true"></i></a>
                   <a target="_blank" href="<?php echo get_post_meta(get_the_ID(), 'wpcf-facebook', true); ?>"><i class="fa fa-facebook fa-top-header fa-lg" aria-hidden="true"></i></a>
                   <a target="_blank" href="<?php echo get_post_meta(get_the_ID(), 'wpcf-twitter', true); ?>"><i class="fa fa-twitter fa-top-header fa-lg" aria-hidden="true"></i></a>
                   <a target="_blank" href="<?php echo get_post_meta(get_the_ID(), 'wpcf-instagram', true); ?>"><i class="fa fa-instagram fa-top-header fa-lg" aria-hidden="true"></i></a>
                   <a target="_blank" href="<?php echo get_post_meta(get_the_ID(), 'wpcf-linkedin', true); ?>"><i class="fa fa-linkedin fa-top-header fa-lg" aria-hidden="true"></i></a></div>

              <?php
                endwhile;
                endif;
              ?>

          </div>
</div>
       
          <div class="col-md-12">
     <div class="col-md-1"></div>
                <?php 
                  $args = array('post_type' => 'supervisor');
                  $performer = new WP_Query($args);
                  if ($performer->have_posts()):
                    while ($performer->have_posts()): $performer->the_post(); 
                    $profile = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID(), 'full'));
                ?>

                  <div class="tp-container col-md-3 col-sm-3 col-xs-6">
                    <img class="team-img-all  hvr-glow" src="<?php echo $profile[0]; ?>"/>  
                    <div class="t-name wow" data-wow-duration="2s"><?php the_title(); ?></div>
                    <div class="designation wow supervisor" data-wow-duration="4s"><?php echo get_post_meta(get_the_ID(), 'wpcf-designation', true) ?></div>
                     <div class="fafa-designation"> <a target="_blank" href="http://localhost/wahsite/contact-us/"><i class="fa fa-mobile fa-top-header fa-lg" aria-hidden="true"></i></a>
                   <a target="_blank" href="<?php echo get_post_meta(get_the_ID(), 'wpcf-facebook', true); ?>"><i class="fa fa-facebook fa-top-header fa-lg" aria-hidden="true"></i></a>
                   <a target="_blank" href="<?php echo get_post_meta(get_the_ID(), 'wpcf-twitter', true); ?>"><i class="fa fa-twitter fa-top-header fa-lg" aria-hidden="true"></i></a>
                   <a target="_blank" href="<?php echo get_post_meta(get_the_ID(), 'wpcf-instagram', true); ?>"><i class="fa fa-instagram fa-top-header fa-lg" aria-hidden="true"></i></a>
                   <a target="_blank" href="<?php echo get_post_meta(get_the_ID(), 'wpcf-linkedin', true); ?>"><i class="fa fa-linkedin fa-top-header fa-lg" aria-hidden="true"></i></a></div>
                    </div>
                <?php
                  endwhile;
                  endif;
                ?>
          </div>
          <div class="col-md-12">
                <?php 
                  $args = array('post_type' => 'hpp');
                  $performer = new WP_Query($args);
                  if ($performer->have_posts()):
                    while ($performer->have_posts()): $performer->the_post(); 
                    $profile = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID(), 'full'));
                ?>
                  <div class="tp-container col-md-3 col-sm-3 col-xs-6">
                    <img class="team-img-all hvr-glow" src="<?php echo $profile[0]; ?>"/>  
                    <div class="t-name wow" data-wow-duration="2s"><?php the_title(); ?></div>
                    <div class="designation wow associate" data-wow-duration="4s"><?php echo get_post_meta(get_the_ID(), 'wpcf-designation', true) ?></div>
                      <div class="fafa-designation"> <a target="_blank" href="http://localhost/wahsite/contact-us/"><i class="fa fa-mobile fa-top-header fa-lg" aria-hidden="true"></i></a>
                   <a target="_blank" href="<?php echo get_post_meta(get_the_ID(), 'wpcf-facebook', true); ?>"><i class="fa fa-facebook fa-top-header fa-lg" aria-hidden="true"></i></a>
                   <a target="_blank" href="<?php echo get_post_meta(get_the_ID(), 'wpcf-twitter', true); ?>"><i class="fa fa-twitter fa-top-header fa-lg" aria-hidden="true"></i></a>
                   <a target="_blank" href="<?php echo get_post_meta(get_the_ID(), 'wpcf-instagram', true); ?>"><i class="fa fa-instagram fa-top-header fa-lg" aria-hidden="true"></i></a>
                   <a target="_blank" href="<?php echo get_post_meta(get_the_ID(), 'wpcf-linkedin', true); ?>"><i class="fa fa-linkedin fa-top-header fa-lg" aria-hidden="true"></i></a></div>
                    
                 
                    </div>
                <?php
                  endwhile;
                  endif;
                ?>
          </div>
          <div class="col-md-12">
          <div class="col-md-1"></div>

                <?php 
                  $args = array('post_type' => 'pip');
                  $performer = new WP_Query($args);
                  if ($performer->have_posts()):
                    while ($performer->have_posts()): $performer->the_post(); 
                    $profile = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID(), 'full'));
                ?>
                  <div class="tp-container col-md-3 col-sm-3 col-xs-6">
                    <img class="team-img-all  hvr-glow" src="<?php echo $profile[0]; ?>"/>  
                    <div class="t-name wow" data-wow-duration="2s"><?php the_title(); ?></div>
                    <div class="designation wow development" data-wow-duration="4s"><?php echo get_post_meta(get_the_ID(), 'wpcf-designation', true) ?></div>
                     <div class="fafa-designation"> <a target="_blank" href="http://localhost/wahsite/contact-us/"><i class="fa fa-mobile fa-top-header fa-lg" aria-hidden="true"></i></a>
                   <a target="_blank" href="<?php echo get_post_meta(get_the_ID(), 'wpcf-facebook', true); ?>"><i class="fa fa-facebook fa-top-header fa-lg" aria-hidden="true"></i></a>
                   <a target="_blank" href="<?php echo get_post_meta(get_the_ID(), 'wpcf-twitter', true); ?>"><i class="fa fa-twitter fa-top-header fa-lg" aria-hidden="true"></i></a>
                   <a target="_blank" href="<?php echo get_post_meta(get_the_ID(), 'wpcf-instagram', true); ?>"><i class="fa fa-instagram fa-top-header fa-lg" aria-hidden="true"></i></a>
                   <a target="_blank" href="<?php echo get_post_meta(get_the_ID(), 'wpcf-linkedin', true); ?>"><i class="fa fa-linkedin fa-top-header fa-lg" aria-hidden="true"></i></a></div>
                    </div>
                <?php
                  endwhile;
                  endif;
                ?>
          </div>
          <div class="col-md-12">
                <?php 
                  $args = array('post_type' => 'nsp');
                  $performer = new WP_Query($args);
                  if ($performer->have_posts()):
                    while ($performer->have_posts()): $performer->the_post(); 
                    $profile = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID(), 'full'));
                ?>
                  <div class="tp-container col-md-3 col-sm-3 col-xs-6">
                    <img class="team-img-all  hvr-glow" src="<?php echo $profile[0]; ?>"/>  
                    <div class="t-name wow" data-wow-duration="2s"><?php the_title(); ?></div>
                    <div class="designation wow associate" data-wow-duration="4s"><?php echo get_post_meta(get_the_ID(), 'wpcf-designation', true) ?></div>
                     <div class="fafa-designation"> <a target="_blank" href="http://localhost/wahsite/contact-us/"><i class="fa fa-mobile fa-top-header fa-lg" aria-hidden="true"></i></a>
                   <a target="_blank" href="<?php echo get_post_meta(get_the_ID(), 'wpcf-facebook', true); ?>"><i class="fa fa-facebook fa-top-header fa-lg" aria-hidden="true"></i></a>
                   <a target="_blank" href="<?php echo get_post_meta(get_the_ID(), 'wpcf-twitter', true); ?>"><i class="fa fa-twitter fa-top-header fa-lg" aria-hidden="true"></i></a>
                   <a target="_blank" href="<?php echo get_post_meta(get_the_ID(), 'wpcf-instagram', true); ?>"><i class="fa fa-instagram fa-top-header fa-lg" aria-hidden="true"></i></a>
                   <a target="_blank" href="<?php echo get_post_meta(get_the_ID(), 'wpcf-linkedin', true); ?>"><i class="fa fa-linkedin fa-top-header fa-lg" aria-hidden="true"></i></a></div>
                    </div>
                <?php
                  endwhile;
                  endif;
                ?>
          </div>
          <div class="col-md-12">
            <div class="col-md-1"></div>
                <?php 
                  $args = array('post_type' => 'op');
                  $performer = new WP_Query($args);
                  if ($performer->have_posts()):
                    while ($performer->have_posts()): $performer->the_post(); 
                    $profile = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID(), 'full'));
                ?>
                  <div class="tp-container col-md-3 col-sm-3 col-xs-6">
                    <img class="team-img-all  hvr-glow" src="<?php echo $profile[0]; ?>"/>  
                    <div class="t-name wow" data-wow-duration="2s"><?php the_title(); ?></div>
                    <div class="designation wow specialist" data-wow-duration="4s"><?php echo get_post_meta(get_the_ID(), 'wpcf-designation', true) ?></div>
                     <div class="fafa-designation"> <a target="_blank" href="http://localhost/wahsite/contact-us/"><i class="fa fa-mobile fa-top-header fa-lg" aria-hidden="true"></i></a>
                   <a target="_blank" href="<?php echo get_post_meta(get_the_ID(), 'wpcf-facebook', true); ?>"><i class="fa fa-facebook fa-top-header fa-lg" aria-hidden="true"></i></a>
                   <a target="_blank" href="<?php echo get_post_meta(get_the_ID(), 'wpcf-twitter', true); ?>"><i class="fa fa-twitter fa-top-header fa-lg" aria-hidden="true"></i></a>
                   <a target="_blank" href="<?php echo get_post_meta(get_the_ID(), 'wpcf-instagram', true); ?>"><i class="fa fa-instagram fa-top-header fa-lg" aria-hidden="true"></i></a>
                   <a target="_blank" href="<?php echo get_post_meta(get_the_ID(), 'wpcf-linkedin', true); ?>"><i class="fa fa-linkedin fa-top-header fa-lg" aria-hidden="true"></i></a></div>
                    </div>
                <?php
                  endwhile;
                  endif;
                ?>
          </div>
  </div>
</div>
<?php get_footer(); ?>