<div class="container section-top">
    <div>
        <div class="page-title wow fadeInUp" data-wow-duration="2s">WIRELESS ACCESS FOR HEALTH</div>
        <div class="intro-description wow fadeInUp" data-wow-duration="2s">WAH is partner to over 100 local governments; helping them manage & improve local healthcare through the use of the wah digital health platform.</div>
    </div>
    <hr/>
    <div class="section-top">
<?php
    $cat_id = get_cat_ID('Features Widget');
    $args = array(
        'cat' => $cat_id,
        'post_type' => 'home-widget',
        'order' => 'ASC'
    );
    $widgets = new WP_Query($args);
    if ($widgets->have_posts()):
    while ($widgets->have_posts()): $widgets->the_post();
        $feat_image = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID(), 'full'));
        ?>
        <div class="col-md-4 col-sm-4 col-xs-4 widget-content hvr-underline-reveal">                    
           <?php if(get_the_title() == 'WAH IN NUMBERS'){ ?>  
           <a href="<?php echo site_url() . '/statistic'; ?>" class="home-widget-icon ">
           <div class="widget-icon">         
                <i class="fa fa-line-chart feature-widget-icons hvr-pulse" aria-hidden="true"></i>
            </div></a>
            <a href="<?php echo site_url() . '/statistics'; ?>" class="home-widget-icon">
            <div class="widget-title"><?php the_title(); ?></div>
            </a>
            <?php }else if(get_the_title() == 'WAH-EMPOWERED SITES'){ ?>
            <a href="<?php echo site_url() . '/map-locations'; ?>" class="home-widget-icon">
            <div class="widget-icon">
                <i class="fa fa-map-marker feature-widget-icons hvr-pulse" aria-hidden="true"></i> 
            </div>  
            </a>
            <a href="<?php echo site_url() . '/map-locations'; ?>" class="home-widget-icon">
            <div class="widget-title"><?php the_title(); ?></div>
            </a>
            <?php }else if(get_the_title() == 'WAH Milestones'){ ?>
            <a href="<?php echo site_url() . '/milestones'; ?>" class="home-widget-icon">
            <div class="widget-icon">
                <i class="fa fa-location-arrow feature-widget-icons hvr-pulse" aria-hidden="true"></i> 
            </div> </a>
            <a href="<?php echo site_url() . '/milestones'; ?>" class="home-widget-icon">
            <div class="widget-title"><?php the_title(); ?></div>
            </a>
            <?php } ?>
                   
            
            <div class="widget-description wow fadeInUp" data-wow-duration="4s"><?php the_excerpt(); ?></div>
        </div>
        <?php
    endwhile;
	endif;
?>
</div>
</div>