<div class="">
<div class="container">

<div class="page-title">WAH People of the Month</div>
<?php 
	$args = array('post_type' => 'feature-of-the-month');
	$performer = new WP_Query($args);
	if ($performer->have_posts()):
    while ($performer->have_posts()): $performer->the_post(); 
		$profile = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID(), 'full'));
?>
 	<div class="tp-container col-md-3">
	  <div class="ih-item circle colored effect5 top-performer-img img-responsive"><a href="#">
        <img class="img" src="<?php echo $profile[0]; ?>"/>  
        <div class="info">
          <div class="info-back">
           
            <p><?php the_content(); ?></p>
          </div>
        </div></a></div>
         <div class="t-name wow text-center"><?php the_title(); ?></div>
         <div class="text-center"><?php echo get_post_meta(get_the_ID(), 'wpcf-position', true); ?></div>
    <!-- end colored -->
		</div>

<?php
	endwhile;
	endif;
?>
</div>
</div>

