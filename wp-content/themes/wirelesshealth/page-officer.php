<?php
/**
 *
 * Template Name:Board of Trustees
 * @package WordPress
 * @subpackage WAH WEBSITE
 * @since WAHSITE 1.0
 */
?>



<?php get_header(); ?>
 <div class="container">

    <?php the_post(); ?>
    <div class="entry-content">
        <p><?php echo the_content();?></p>
<?php
$args = array(
    'post_type' => 'board-of-trustee',
    'order' => 'DESC'
);

$widgets = new WP_Query($args);
if ($widgets->have_posts()):
while ($widgets->have_posts()): $widgets->the_post();
  
?>


<div class="container col-md-6">

 <hr>
    <div> 
        <?php the_post_thumbnail('medium', array('class' => 'img-college')); ?>
    </div>

       
        <div class=" text-center title1"><?php the_title(); ?></div>
         <div class="college-text text-justify"> <?php echo get_post_meta(get_the_ID(), 'wpcf-colleges', true) ?></div>
        <div class="sub-content"><?php echo get_post_meta(get_the_ID(), 'wpcf-subtitle', true) ?></div>
        <div class="wow fadeInRight" data-wow-duration="2s">
            <?php the_content(); ?>
        </div>
     
  

</div>

<?php
   
endwhile;
endif;
?>
</div>


<?php get_footer(); ?>