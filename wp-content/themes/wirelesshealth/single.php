<?php get_header(); ?>

<div class="container">
    <div class="col-md-9">


        <?php the_post(); ?>

        <div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
        
            <div class="entry-content">
                <?php the_content(); ?>
                <?php wp_link_pages('before=<div class="page-link">' . __( 'Pages:', 'hbd-theme' ) . '&after=</div>') ?>
            </div><!-- .entry-utility -->
            <div class="nav-below text-right">
                    <?php previous_post_link( '%link', '<span class="meta-nav">&laquo;</span> %title' ) ?> <span style="color: #fff;">&#8226;</span>  <?php next_post_link( '%link', '%title <span class="meta-nav ">&raquo;</span>' ) ?>
            </div><!-- #nav-below -->     
            
        </div><!-- #content -->
    </div>
    <div class="col-md-3 sub-header-bg">
        <?php get_sidebar(); ?>
    </div>
</div><!-- #container -->
<?php get_footer(); ?>