<?php
/**
 *
 * Template Name: Join Us
 * @package WordPress
 * @subpackage WAH WEBSITE
 * @since WAHSITE 1.0
 */
?>


<?php get_header(); ?>
 <div class="container">

    <?php the_post(); ?>
    <div class="entry-content">
        <p><?php echo the_content();?></p>
<?php
$args = array(
    'post_type' => 'wah-careers',
    'order' => 'ASC'
);

$widgets = new WP_Query($args);
if ($widgets->have_posts()):
while ($widgets->have_posts()): $widgets->the_post();
  
?>


<div class="container">
 <div class="row officer-list careers-bg">
 <hr>
    <div class="col-md-5 about-description"> 
        <?php the_post_thumbnail('medium', array('class' => 'img-responsive about-widget-img hvr-bob ')); ?>
    </div>
    <div class="col-md-7 about-widget-content">
        <div class="page-title text-left"><?php the_title(); ?></div>
        <div class="sub-content"><?php echo get_post_meta(get_the_ID(), 'wpcf-subtitle', true) ?></div>
        <div class="wow fadeInRight" data-wow-duration="2s">
            <?php the_content(); ?>
        </div>
     
    </div>
    </div>
</div>

<?php
   
endwhile;
endif;
?>
</div>


<?php get_footer(); ?>