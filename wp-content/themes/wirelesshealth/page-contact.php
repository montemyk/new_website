<?php
/**
 *
 * Template Name: CONTACT US
 * @package WordPress
 * @subpackage WAH WEBSITE
 * @since WAHSITE 1.0
 */
?>



<?php get_header(); ?>
 

<div class="container">
  <div class="post-feature-img">
    <?php the_post_thumbnail('full', array('class' => 'img-responsive')); ?>
  </div>
  <?php the_post(); ?>
  <div class="entry-content">
    <div class="row dashboard-stat">
      <div class="col-md-12 contactusadd">       
        <div><i class="fa fa-grey fa-map-marker fa-2x fa-align-center statsheader" aria-hidden="true"></i>  2/F Room 201 TPHO Dormitory, Hospital Drive, San Vicente, Tarlac City 2300, Philippines</div>
         <div class="col-md-6 infocontactus">
        <div><i class="statsheader" aria-hidden="true"></i> <b>General Inquiries </b></div>

      </div>
      <div class="col-md-6 infocontactus1">
        <div><i class="fa fa-grey fa-envelope-o fa-lg statsheader" aria-hidden="true"></i>  Email: <u class="mailcolor">wah.pilipinas@gmail.com</u></div>
        <div><i class="fa fa-grey fa-phone fa-lg statsheader" aria-hidden="true"></i>  Phone: <u class="mailcolor">(045) 982-1246</u></div>
      </div>
       <div class="col-md-6 infocontactus">
        <div><i class="statsheader" aria-hidden="true"></i> <b>Billing and Contracts Inquiries</b></div>

      </div>
      <div class="col-md-6 infocontactus1">
        <div><i class="fa fa-grey fa-female fa-lg statsheader" aria-hidden="true"></i>  Rose Ann Biag  | <u class="mailcolor">roseann.biag@gmail@gmail.com</u></div>
        <div><i class="fa fa-grey fa-female fa-lg statsheader" aria-hidden="true"></i>Jhuvy Dizon  |<u class="mailcolor">jhuvydizon08@gmail.com </u></div>
      </div>
       <div class="col-md-6 infocontactus">
        <div><i class="statsheader" aria-hidden="true"></i> <b>Training Inquiries </b></div>

      </div>
      <div class="col-md-6 infocontactus1">
        <div><i class="fa fa-grey fa-male fa-lg statsheader" aria-hidden="true"></i> Francis Joseph Gamboa  |<u class="mailcolor">francisgamboa30@gmail.com</u></div>
        </div>
      </div>
       <div class="col-md-6 infocontactus">
        <div><i class="statsheader" aria-hidden="true"></i> <b>Technical Inquiries </b></div>

      </div>
      <div class="col-md-6 infocontactus1">
        <div><i class="fa fa-grey fa-male fa-lg statsheader" aria-hidden="true"></i>Macquel Serrano  | <u class="mailcolor">macquelserrano@gmail.com</u></div>
      </div>
      </div>
       <div class="col-md-6 infocontactus">
        <div><i class="statsheader" aria-hidden="true"></i> <b>Media Inquiries </b></div>

      </div>
      <div class="col-md-6 infocontactus1">
        <div><i class="fa fa-grey fa-female fa-lg statsheader" aria-hidden="true"></i>Precious Joan Narciso  | <u class="mailcolor">pjdnarciso@gmail.com</u></div>
        <div><i class="fa fa-grey fa-female fa-lg statsheader" aria-hidden="true"></i> Maria Cristiana Santos  |<u class="mailcolor">mariacristiana@gmail.com</u></div>
      </div>
       

     </div>   
  </div>      
</div>
<div class="clearfix"></div>
<?php get_footer(); ?>

