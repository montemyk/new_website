<?php
/**
 *
 * Template Name: Awards
 * @package WordPress
 * @subpackage WAH WEBSITE
 * @since WAHSITE 1.0
 */
?>



<?php get_header(); ?>
<div class="container">

  <?php the_post(); ?>
  <div class="entry-content"></div>

    <?php
    $args = array(
      'post_type' => 'award',
      'order' => 'DESC'
      );

    $widgets = new WP_Query($args);
    if ($widgets->have_posts()):
      while ($widgets->have_posts()): $widgets->the_post();
    {
      ?>
          <div class="row">
             <hr>
             <div class="col-md-4 awardspace"> 
              <?php the_post_thumbnail('medium', array('class' => 'img-responsive about-widget-img hvr-bob ')); ?>
            </div>
            <div class="col-md- about-widget-content">
              <div class="page-title text-left"><?php the_title(); ?></div>
              <div class="wow fadeInRight" data-wow-duration="2s">
                <?php the_content(); ?>
              </div>

            </div>

          </div>

      <?php
    }
    endwhile;
    endif;
    ?>

<div class="clearfix"></div>

<hr>
<div class="container">
  <?php
    $args = array(
      'post_type' => 'conference',
      );

    $widgets = new WP_Query($args);
    if ($widgets->have_posts()):
      while ($widgets->have_posts()): $widgets->the_post();
    {
      ?>
<div class="col-md-3">
  
       
              <?php the_post_thumbnail('medium', array('class' => 'img-responsive about-widget-img')); ?>
                <div class="text-center"><?php the_title(); ?></div>
            
        </div>
          
      

      <?php
    }
    endwhile;
    endif;
    ?>
</div>
</div>


<?php get_footer(); ?>