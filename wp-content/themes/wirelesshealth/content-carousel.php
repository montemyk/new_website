
    <script type="text/javascript">  
      
      jQuery(function($){
        
        $('.circle').mosaic({
          opacity   : 0.8     //Opacity for overlay (0-1)
        });
        
        $('.fade').mosaic();
        
        $('.bar').mosaic({
          animation : 'slide'   //fade or slide
        });
        
        $('.bar2').mosaic({
          animation : 'slide'   //fade or slide
        });
        
        $('.bar3').mosaic({
          animation : 'slide',  //fade or slide
          anchor_y  : 'top'   //Vertical anchor position
        });
        
        $('.cover').mosaic({
          animation : 'slide',  //fade or slide
          hover_x   : '400px'   //Horizontal position on hover
        });
        
        $('.cover2').mosaic({
          animation : 'slide',  //fade or slide
          anchor_y  : 'top',    //Vertical anchor position
          hover_y   : '80px'    //Vertical position on hover
        });
        
        $('.cover3').mosaic({
          animation : 'slide',  //fade or slide
          hover_x   : '400px',  //Horizontal position on hover
          hover_y   : '300px'   //Vertical position on hover
        });
        
        });
        
    </script>
    
                <style type="text/css">
                
                  /*Demo Styles*/
                  
                    #content{ width:100%; }
                    .margin-story{margin-left: -20px;}
                    
                    .clearfix{ display: block; height: 0; clear: both; visibility: hidden; }
                    .title-mainstory{color: #fff;
                      font-family: Arial;
                    } 
                 
                
                </style>   <div id="content" class="margin-story">
                   <?php 
              $args = array('post_type' => 'carousel');

                  $slides = new WP_Query($args);
                  if ($slides->have_posts()):
                          while ($slides->have_posts()): $slides->the_post();
            ?>

<div class="container col-md-4">

              <a href="<?php the_permalink(); ?>"> 
          <div class="mosaic-block bar2">
                        
              <div class="mosaic-backdrop banner-story-img img-responsive">
                          <?php 
                            $feat_image = wp_get_attachment_url(get_post_thumbnail_id());
                          ?>
                         
                    <img src="<?php echo $feat_image; ?>" />
                        <div class="mosaic-overlay">
                           <h6><?php the_title(); ?></h6>  </a>    
                                <p>
                                  <?php the_excerpt();  ?>
                               
                                </p>
                           
                        </div>  
                       
                                
              </div>
                   
        </div>
     
  </div>
                  <?php 
                        endwhile;
                        endif;
                        ?>
                        <div class="clearfix"></div>
       
</div>