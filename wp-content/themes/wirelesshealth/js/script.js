$(document).ready(function () {

      $(function () {

        $('.level a[title]').tooltip();
        $('.level').tabs();


        $('a[href*="#"]:not([href="#"])').click(function () {
            if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
                var target = $(this.hash);
                target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
                if (target.length) {
                    $('html, body').animate({
                        scrollTop: target.offset().top
                    }, 1000);
                    return false;
                }
            }
        });

        var data = {
            labels: [
                "Luzon",
                "visayas",
                "Mindanao"
            ],
            datasets: [
                {
                    data: [300, 50, 100],
                    backgroundColor: [
                        "#3498DB",
                        "#9B59B6",
                        "#E74C3C"
                    ],
                    hoverBackgroundColor: [
                        "#3498DB",
                        "#9B59B6",
                        "#E74C3C"
                    ]
                }]
        };
        window.onload = function() {
            var ctx = document.getElementById("chart-doughnat").getContext("2d");
            ctx.moveTo(0,0);
            //Chart.defaults.global.legend.display = false;
            var myChart = new Chart(ctx,{
                type:"doughnut",
                data: data,
                animation:{
                    animateScale:true
                }
            });
            //document.getElementById('js-legend').innerHTML = myChart.generateLegend();
            window.myDoughnut = myChart;


        };
    });

    $('.bxslider').bxSlider();
    $('.partners').slick({
      infinite: true,
      dots: false,
      arrows: true,
      autoplay: true,
     autoplaySpeed: 5000,
      slidesToShow: 6,
      slidesToScroll: 6,
      responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 1,
            infinite: true,
            dots: false
          }
        },
        {
          breakpoint: 600,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 1
          }
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1
          }
        }
      ]
    });

    $('.bxslider-testimonial').bxSlider({
        mode: 'fade',
        easing: 'swing',
        auto: true,
        autoControls: true,
        pause: 2000
    });

      var wow = new WOW(
      {
        boxClass:     'wow',      // animated element css class (default is wow)
        animateClass: 'animated', // animation css class (default is animated)
        offset:       0,          // distance to the element when triggering the animation (default is 0)
        mobile:       true,       // trigger animations on mobile devices (default is true)
        live:         true,       // act on asynchronously loaded content (default is true)
        callback:     function(box) {
          // the callback is fired every time an animation is started
          // the argument that is passed in is the DOM node being animated
        },
        scrollContainer: null // optional scroll container selector, otherwise use window
      }
    );
    wow.init();




});



