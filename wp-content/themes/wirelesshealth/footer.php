    </div><!-- #main -->
 
    <footer class="container-fluid no-padding">
    	<div class="container footer-widget">
    		<div class="row">
	    		<div class="col-md-12">
	    			            <a href="<?php echo site_url(); ?>">
                <img src="<?php echo get_bloginfo('template_directory');?>/img/wah_logo7.png" class="wah-logofooter" />
            </a>
            		
			    	      <p>2/F Room 201 TPHO Dormitory, Hospital Drive, 
						San Vicente, Tarlac City 2300, Philippines </p>
	    		</div>
	    			
	    			<div> <ul>
					<li><a target="_blank"  href="https://www.facebook.com/wah.ph/"><i class="fa fa-facebook fa-2x" aria-hidden="true"></i></a></li>
					<li><a target="_blank"  href="https://www.instagram.com/wah_team/"><i class="fa fa-instagram fa-2x" aria-hidden="true"></i></a></li>
                    <li><a target="_blank"  href="https://twitter.com/wah_team"><i class="fa fa-twitter fa-2x" aria-hidden="true"></i></a></li>
                    <li><a target="_blank"  href="https://www.wah.pilipinas@gmail.com"><i class="fa fa-envelope fa-2x" aria-hidden="true"></i></a></li>
                    <li><a target="_blank"  href="https://www.linkedin.com/company/6594007?trk=tyah&trkInfo=clickedVertical%3Acompany%2CclickedEntityId%3A6594007%2Cidx%3A3-1-6%2CtarId%3A1473903817787%2Ctas%3AWireless%20Access%20"><i class="fa fa-linkedin fa-2x" aria-hidden="true"></i></a></li>
						</ul>
						</div>
	    				<p><b>Mobile:</b> 09985651432/ 09999999999</p>
						<p class="footertel"><b>Telephone:</b> (045) 982-1246</p>

						
	    			</div>
	    		</div>	    		
	    	</div>
	    	<div class="copyright">2016. Wireless Access for Health. All Rights Reserved.</div>
    	</div>
    </footer>
<?php wp_footer(); ?>
</div>
</body>
</html>