

<div class="container test">
	<div class="page-title">Our Partner</div>
	
	<ul class="bxlsider">
	<div class="title-space partners">
		<?php
		    $args = array(
		        'post_type' => 'partner',
		        'order' => 'ASC'
		    );
		    $widgets = new WP_Query($args);
		    if ($widgets->have_posts()):
		    while ($widgets->have_posts()): $widgets->the_post();
		        $feat_image = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID(), 'full'));
		        ?>
		        <div class="">                   
		            <img class="img-responsive hvr-pulse-shrink" src="<?php echo $feat_image[0]; ?>"/>  

		        </div>
		        <?php
		    endwhile;
			endif;
		?>
	</div>
	</div>

<div id="gridSystemModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="gridModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="gridModalLabel">Luzon LGU's</h4>
      </div>
      <div class="modal-body">
        <div class="container-fluid ">
			         
			 <?php 
				$args = array('orderby' => 'title', 'order' => 'ASC', 'post_type' => 'luzon-partners');
				
				$lgu = new WP_Query($args);
				if ($lgu->have_posts()):
			    while ($lgu->have_posts()): $lgu->the_post(); 
					$profile = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID(), 'full'));
			?>
			<div class="tp-container col-md-4 col-sm-3 col-xs-6">
			<img class="lgu-partner-img hvr-glow" src="<?php echo $profile[0]; ?>"/>  
			<div class="t-name wow"><?php the_title(); ?></div>
			</div>
					        <?php
					    endwhile;
						endif;
					?>
			          
          
        
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
       
      </div>
    </div>
  </div>
</div>
<div class="bd-example bd-example-padded-bottom  btmposition">
 
</div>
<div id="gridSystemModal1" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="gridModalLabel1" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="gridModalLabel1">Visayas LGU's</h4>
      </div>
      <div class="modal-body">
        <div class="container-fluid ">
			         
			 <?php 
				$args = array('orderby' => 'title', 'order' => 'ASC', 'post_type' => 'visayas-partner');
				
				$lgu1 = new WP_Query($args);
				if ($lgu1->have_posts()):
			    while ($lgu1->have_posts()): $lgu1->the_post(); 
					$profile = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID(), 'full'));
			?>
			<div class="tp-container col-md-4 col-sm-3 col-xs-6">
			<img class="lgu-partner-img hvr-glow" src="<?php echo $profile[0]; ?>"/>  
			<div class="t-name wow"><?php the_title(); ?></div>
			</div>
					        <?php
					    endwhile;
						endif;
					?>
			          
          
        
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
       
      </div>
    </div>
  </div>
</div>
<div class="bd-example bd-example-padded-bottom  btmposition">
 </div>
</div>
<!-- include jQuery -->
</div>
<div class="container">
<div class="text-center">
<a class="col-md-4 luzon" href="" data-toggle="modal" data-target="#gridSystemModal">Luzon</a>
<a class="col-md-4 visayas" href="" data-toggle="modal" data-target="#gridSystemModal1">Visayas</a>
<a class="col-md-4 mindanao"  href="#">Mindanao</a>
</div>
</div>
